# kdocgen — Documentation Generator for KiCad Projects

This is a Python script that leverages the `kicad-cli` tool introduced in KiCad 7 to output full sets of documentation and Gerber files for KiCad electronics design projects.

It's based on [kicad_7_cli_doc](https://github.com/JordanAceto/kicad_7_cli_doc_gen) by Jordan Aceto. It has been extensively modified to make it faster and more flexible to use.

It is intended for KiCad version 7 under Linux operating systems. It runs as a standalone script from the Linux command line.

## What it does

With a single command line, kdocgen can generate any or all of:

- Schematic PDF
- Interactive HTML BOM
- Static BOM
- Gerbers and drill files, with or without compression
- PCB layers as PDFs
- Front and back of PCB as SVG
- STEP file of PCB
- Simple README file

for each of every KiCad project associated with a single main project directory.

It is controlled by parameters read from configuration files at several levels for maximum flexibility. Leveraging [KiKit](https://yaqwsx.github.io/KiKit/v1.3/), it can generate separate outputs for individual PCBs in a multi-board project. Either KiKit or kicad-cli can be used for Gerber file generation. For speed and minimal file churn, no file is regenerated if there already is a file that is newer than the KiCad file it was generated from.

## Dependencies

* KiCad version 7.06 or above
* Python3
* The following Python libraries: `argparse`, `fnmatch`, `os`, `json`, `copy`, and `shutil`. All are likely to be standard in a Python3 installation.

There are two optional dependencies:

- [Interactive HTML BOM](https://github.com/openscopeproject/InteractiveHtmlBom) if you want to generate interactive BOMs.
- [KiKit](https://yaqwsx.github.io/KiKit/v1.3/) if you want to have separate outputs for multi-PCB projects, or if you want to use it instead of kicad-cli for Gerber generation.

You would need to install these separately if you want to use them.

## Alternatives

* A makefile based approach: [https://github.com/tuna-f1sh/kicad-makefile](https://github.com/tuna-f1sh/kicad-makefile).

## Usage

```
usage: kdocgen.py [-h] [-v] [-p] [-c] [-f FILESMADE] [-fi FILESMADE_IGNORE]
                  project_dir

positional arguments:
  project_dir           path to directory encompassing one or more KiCad
                        projects

options:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -p, --params          show parameters for each KiCad project and exit
  -c, --cont            continue processing after error
  -f FILESMADE, --filesmade FILESMADE
                        file to which to write list of files created
  -fi FILESMADE_IGNORE, --filesmade_ignore FILESMADE_IGNORE
                        like --filesmade but omit files ignored by git
```

## Getting started

Included in this repo is a directory called `example_board`, which contains a directory called `kicad_7`, which contains files for a KiCad project including `fv1_board.kicad_pro`, `fv1_board.kicad_sch`, `fv1_board.kicad_pcb`, and so on. Copy `example_board` and its contents to another location, if you want to avoid modifying the repo. `example_board` is our *main project directory* and it contains (at a lower folder level) `kicad_7`, a *KiCad project* (or *k-project*) *directory*. `example_board` is the name of the main project and `fv1_board` is the name of the k-project.

Enter this command:
```
$ python3 kdocgen.py path/to/example_board
```

A bunch of output lines should appear, after which you should find some new things in `example_board`:

* A directory named `example_board/docs`, containing one file, `fv1_board_schematic.pdf`, which is a PDF of the schematic diagram; and three directories, `example_board/docs/2D`, `example_board/docs/3D`, and `example_board/docs/BOM`.
    * Inside `example_board/docs/2D` should be a number of PDF files, one showing the layout for each of the copper, mask, paste, and silkscreen layers (with edge layer included in each), and two SVG files, representing the front and back of the PCB.
    * Inside `example_board/docs/3D` should be `fv1_board.step`, a 3D model of the PCB with its components.
    * Inside `example_board/docs/BOM` should be `fv1_board_bom.csv`, a CSV format bill of materials.
* A directory named `example_board/Gerbers`. It contains `fv1_board_gerbers.zip`, a compressed archive of Gerber files for the board.
* A file named `README.md`, a very simple README file in Markdown format — it just contains the project name, the k-project name, and the SVG images of the board.

One thing you probably will *not* find is an interactive BOM. Among the outputs from `kdocgen.py` you are likely to see the line

```
*** /usr/local/bin/generate_interactive_bom.py not found, skipping
```

unless of course  you do have that Python script, or a symlink to it, stored there. We'll get back to this. Suffice to say that once the path problem is dealt with, an interactive BOM file named `fv1_board_ibom.html` will be generated in the `example_board/docs/BOM` folder.

If you immediately enter the same command again, you will get only a couple of lines of output and it will finish in an instant. This is because it sees all the files it generated before are still newer than the KiCad files they were generated from, so there is no point in re-doing them. If you were to make a change to the PCB, but *not* any change to the schematic, and then re-run `kdocgen.py`, it would re-generate  the schematic PDF and the CSV BOM but not any of the other files. Conversely, if you were to modify the schematic file but make no change to the PCB, and then re-run `kdocgen.py`, the other files would be regenerated and the schematic PDF and the CSV BOM would not. (The interactive BOM is *not* generated from the schematic, like static BOMs, but from the PCB.)

The `README.md` file is a special case; `kdocgen.py` will *not* generate one if there is any README file already, regardless of how old it is.

## Multiple KiCad projects

Your main project directory might contain — one or more levels down — more than one directory with KiCad files in it. That is, your main project might include multiple related k-projects. For instance, the main project might be for a synthesizer module, and there might be one KiCad project for the PCB and another for an FR4 front panel. `kdocgen.py` will find all the KiCad projects under the main project directory and process all of them. Go ahead and add a second KiCad project to `example_board` and try it. Just make sure it has a different name than `fv1_board`. The k-project name appears in the output file names, so you know which belongs to what.

Again, `README.md` is obviously an exception to that; if generated, it will contain names and images of all the k-projects that were processed.

## Configuration files

All this is okay, but it's designed to cater to one person's particular choice of directory names and directory structure, BOM format, choice of layers to export, and so forth — not to mention the probably incorrect ibom script path. If you want to change any of these, you could modify the Python code, but the better way is to use configuration files.

Inside the script, a lot of parameters are set to default values, but if a JSON format file called `kdocgenrc.json` (or `.kdocgenrc.json`, with the unhidden file overriding any hidden file) exists in your home directory, the parameter values stored in it can override those defaults any time you run `kdocgen.py`. We call this the *global configuration file*. But if there is a file of the same name in the main project directory — a *semi local configuration file* — then it will override the defaults and the global file when you run `kdocgen.py` on that main project. And if there is a file of the same name in a k-project directory — a *local configuration file* — then it will override the defaults and the global file and the semi-local file during processing of that one k-project.

That behavior is useful because you may want different outputs for one project or for one k-project than for another. In the situation where there is a PCB k-project and a front panel k-project, you probably have no interest in a schematic diagram or a BOM for the front panel! You can disable those outputs for the panel, while keeping them for the PCB, by using a local configuration file in the front panel k-project directory like

```
{
    "schem_pdf": {"enabled": false},
    "ibom": {"enabled": false},
    "bom": {"enabled": false},
    "step": {"enabled": false}
}
```

which tells `kdocgen.py` not to generate schematics, BOMs, or STEP files. This overrides the setting of `enabled` from the defaults and the global and semi-local files. The other parameters, which are not mentioned in this file, retain the settings established by the other configuration files.

To fix the ibom path problem, find the path to the ibom executable. On my installation I found it in `/home/rsholmes/.local/share/kicad/7.0/3rdparty/plugins/org_openscopeproject_InteractiveHtmlBom/generate_interactive_bom.py`, which I definitely would symlink to `ibom.py` in a directory in my `$PATH` if I were regularly using it on the command line; but for `kdocgen.py` I just have to have a section in my global configuration file:

```
    "ibom": {
        "path": "/home/rsholmes/.local/share/kicad/7.0/3rdparty/plugins/org_openscopeproject_InteractiveHtmlBom/generate_interactive_bom.py",
        "dark_mode": false
    },
```

and that takes care of the path and [turns off](https://gizmodo.com/dark-mode-is-for-suckers-1838544708) dark mode.

A complete sample configuration file is included as `kdocgenrc.json`. You can copy this to `~/kdocgenrc.json` or `~/.kdocgenrc.json` and modify any entries you want to, and then use cut-down versions in semi-local and local configuration files as needed. A detailed layout of all the options may be found in [README_config_files.md](README_config_files.md).

## Multi-board projects

Often a single circuit will be implemented on two or more PCBs, for instance to stack them to reduce the total width, or to accommodate different height board mounted jacks and controls. You can lay out multiple PCBs in a single KiCad PCB file, but KiCad has no support built in for exporting those PCBs to separate output files. The KiKit package provides a workaround for that (among other things), and `kdocgen.py` can leverage it to give you documentation and Gerbers for individual circuit boards. Like Interactive BOM, if you want to use KiKit, you need to install it, and then put its path in your global configuration file:

```
    "kikit_path": "/home/rsholmes/.local/bin/kikit"
```

Having done that, then if for instance a PCB file called `project.kicad_pcb` file has two PCBs laid out, marked with board mark footprints with references B1 and B2, then a local configuration file looking like

```
{
    "separation": [
	["Front_PCB", "annotation; ref: B1"],
	["Back_PCB", "annotation; ref: B2"]
	]
}
```

will tell `kdocgen.py` to split the k-project into two temporary k-projects, called `project_Front_PCB` and `project_Back_PCB`, the first for the PCB marked `B1` and the second for the PCB marked `B2`; then generate separate ibom, layout PDF and SVG files, STEP, files, and Gerbers, and separate images in the README; then delete the temporary k-project files. (The outputs that derive from the schematic file — the schematic PDF and the static BOM — will be single files covering the whole schematic.) You can also use something like `"rectangle; tlx: 89mm; tly: 89mm; brx: 111mm; bry: 111mm"` to specify a board by its bounding box — see the KiKit documentation for details.

## Command line options

The `-h` or `--help` option prints a usage message.

The `-v` or `--version` option prints the version number and exits.

The `-p` or `--params` option prints, for each k-project, the complete list of parameters it would use to generate documentation (taking into account the global, semi local, and local config files), and then exits.

The `-c` or `--cont` option forces `kdocgen` to continue processing even if a step results in an error. Without this option it terminates (with status 1) after the first error.

The `-f FILESMADE` or `--filesmade FILESMADE` option causes `kdocgen` to write a list of the paths to the files it created (or replaced) to the file at path `FILESMADE`. (Not including the specified file!) If a directory of Gerber files was created, a single line in this list gives the path to that directory; the individual Gerber files are not listed. This list is suitable for use with the `--pathspec-from-file` option of `git add`. If `FILESMADE` already exists it is renamed to `FILESMADE.bak` before writing the new version.

The `-fi FILESMADE_IGNORE` or `--filesmade_ignore FILESMADE_IGNORE` option does the same, except that it omits any file that is ignored by git (as determined by running `git check-ignore` from your *current* directory).

## kcommit.sh

The shell script `kcommit.sh` can be used in place of `git commit`, e.g.

```
kcommit.sh -m "Fix incorrectly connected pot"
```
It first runs `kdocgen.py` on the current directory. If any files are created or re-created as a result they are staged with `git add`. Then `git commit` is called with the arguments given to `kcommit.sh`.

(I did try to do this with `.git/hooks/pre-commit` but could not get that to work quite right. Deleted `kdocgen.py` files would get re-generated but not committted.)
