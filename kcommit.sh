#!/bin/bash

# Usage:

# kcommit [git commit args]

# Do kdocgen and `git add` its resulting files before doing `git
# commit` with arguments passed from command line invoking
# this. (Files ignored by git are not added.)

fn="filesmade.txt"
kdocgen.py --filesmade_ignore $fn .
if [ ! -f $fn ]; then
    echo "***" $fn " does not exist, proceeding"
else
    # Test for empty file
    if [ -s $fn ]; then
	echo "*** Files made:"
	cat $fn
	echo ""
	git add --pathspec-from-file=$fn
    fi
    rm $fn
fi
git commit "$@"
